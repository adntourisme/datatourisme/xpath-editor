/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
  .module('xpath-editor')
  .component('xpathInput', {
      template: require('./xpath-input.html'),
      require: {
          ngModelCtrl: 'ngModel'
      },
      controller: controller,
      bindings: {
          "schema": "<",
          "context": "<",
          "selector": "@",
          "placeholder": "@",
          "mode": "@"
      }
  });

/**
 * @ngInject
 */
function controller(bsNgDialog) {
    var ctrl = this;
    this.open = function () {
        var dialog = bsNgDialog.open({
            closeByDocument: false,
            closeByEscape: false,
            template: require('./xpath-editor-modal.html'),
            plain: true,
            controller: ['$scope', function($scope) {
                $scope.xpath = angular.copy(ctrl.ngModelCtrl.$modelValue);
                $scope.context = ctrl.context;
                $scope.schema = ctrl.schema;
                $scope.selector = ctrl.selector;
                $scope.mode = ctrl.mode;
            }]
        });

        dialog.closePromise.then(function(dialog) {
            if(dialog.value) {
                ctrl.ngModelCtrl.$setViewValue(dialog.value);
            }
        });
    };


}


