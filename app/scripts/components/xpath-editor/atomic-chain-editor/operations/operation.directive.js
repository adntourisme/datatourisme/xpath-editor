/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('xpath-editor')
    .directive('atomicOperation', atomicOperationDirective);

/**
 * @ngInject
 */
function atomicOperationDirective($compile) {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            name: "=atomicOperation",
            args: "=ngModel"
        },
        link: link
    };

    /**
     * @ngInject
     */
    function link(scope, elem, attrs) {
        var template = '<atomic-operation-' + scope.name + ' ng-model="args" />',
            compiled = $compile(template)(scope);
        elem.append(compiled);
    }
}