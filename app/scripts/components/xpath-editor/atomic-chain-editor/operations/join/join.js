/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('xpath-editor')
    .component('atomicOperationJoin', {
        template: require("./join.html"),
        require: {
            ngModelCtrl: 'ngModel'
        }
    });