/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
    .module('xpath-editor')
    .directive('xexpr', xexprDirective);

var parser = require("../../parsers/xexpr");

/**
 * @ngInject
 */
function xexprDirective() {
    return {
        restrict: "A",
        require: "ngModel",
        link: link,
        priority: 1
    };

    /**
     * @ngInject
     */
    function link(scope, elem, attrs, ctrl) {
        var mode = attrs['xexpr'] ? attrs['xexpr'] : undefined;

        //format text going to user (model to view)
        ctrl.$formatters.push(function(value) {
            return value && (!mode || value.type == mode) ? value.expr : undefined;
        });

        //format text from the user (view to model)
        ctrl.$parsers.push(function(value) {
            if(!value) {
                return null;
            }
            var parsed = parser.parse(value);
            if(!parsed) {
                return undefined;
            }

            if(mode && parsed.type != mode) {
                return undefined;
            }
            return parsed;
        });
    }
}
