/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
/*
'use strict';

var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');

/!**
 * Variables
 *!/
var srcDir = './app';
var entryFiles = [srcDir + '/!*.html'];
var dstDir = './dist';


/!**
 * @param minify
 * @param dest
 * @returns {*}
 *!/
function buildHtml(dest) {
    return gulp.src(entryFiles)
        //.pipe($.angularHtmlify())
        .pipe(htmlmin({
            removeComments: true,
            collapseWhitespace: true
        }))
        .pipe(gulp.dest(dest));
}

/!**
 * @type {{build: module.exports.build}}
 *!/
module.exports = {
    build: function() {
        return buildHtml(dstDir);
    }
};*/
